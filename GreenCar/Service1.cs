﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using GreenCar.IO.FTP;

namespace GreenCar
{
    public partial class Service1 : ServiceBase
    {
        static string caminhoPasta = "c:\\AuxBkpSgs";

        public static string getCaminhoPastaMonitorada() 
        {
            if (!Directory.Exists(caminhoPasta))
            {
                Directory.CreateDirectory(caminhoPasta);
            }

            return caminhoPasta;
        }

        public static string getUsuarioFtp() 
        {
            return "usuario";
        }

        public static string getSenhaFtp() 
        {
            return "asd-1234";
        }

        public static string getCaminhoFtp() 
        {
            return "ftp://localhost";
        }

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            log("INICIO SERVICO: " + DateTime.Now.ToString());
            MonitorarPasta();
        }

        protected override void OnStop()
        {
            log("FIM SERVICO: " + DateTime.Now.ToString());
        }

        private static void MonitorarPasta() 
        {
            FileSystemWatcher fsw = new FileSystemWatcher();
            fsw.Path = getCaminhoPastaMonitorada();
            fsw.Created += arquivoCriado;

            fsw.EnableRaisingEvents = true;
        }

        private static void arquivoCriado(object sender, FileSystemEventArgs e)
        {
            log("ARQUIVO CRIADO: " + e.FullPath);

            FTP.EnviarArquivoFTP(e.FullPath, getCaminhoFtp(), getUsuarioFtp(), getSenhaFtp());

            File.Delete(e.FullPath);
        }

        private static void log(string str)
        {
            StreamWriter sw = new StreamWriter("c:\\logGreenCar.txt", true);

            sw.WriteLine(DateTime.Now.ToString() + " -> " + str);
            sw.Flush();
            sw.Close();
        }
    }
}
