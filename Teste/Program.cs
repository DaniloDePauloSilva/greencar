﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using GreenCar.IO.FTP;

namespace Teste
{
    class Program
    {
        static string caminhoPasta = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "/AuxBkpSgs";
        
        static void Main(string[] args)
        {
            Console.WriteLine("CAMINHO MONITORADO: " + caminhoPasta);

            if (!Directory.Exists(caminhoPasta))
            {
                Directory.CreateDirectory(caminhoPasta);
            }

            MonitorarPasta();

            Console.ReadKey(false);
        }

        public static string getCaminhoPastaMonitorada()
        {
            return caminhoPasta;
        }

        public static string getUsuarioFtp()
        {
            return "usuario";
        }

        public static string getSenhaFtp()
        {
            return "asd-1234";
        }

        public static string getCaminhoFtp()
        {
            return "ftp://localhost";
        }

        private static void MonitorarPasta()
        {
            FileSystemWatcher fsw = new FileSystemWatcher();
            fsw.Path = getCaminhoPastaMonitorada();
            fsw.Created += arquivoCriado;

            fsw.EnableRaisingEvents = true;
        }

        private static void arquivoCriado(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine("ARQUIVO CRIADO!");
            Console.WriteLine(e.FullPath);
            FTP.EnviarArquivoFTP(e.FullPath, getCaminhoFtp(), getUsuarioFtp(), getSenhaFtp());

            File.Delete(e.FullPath);
        }
    }
}
